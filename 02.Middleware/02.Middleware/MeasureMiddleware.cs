﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace _02.Middleware
{
    public class MeasureMiddleware
    {
        private readonly RequestDelegate next;
        private readonly Stopwatch stopWatch;

        public MeasureMiddleware(RequestDelegate next)
        {
            this.next = next;
            this.stopWatch = new Stopwatch();
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (context.Request.Query.ContainsKey("measure") &&
                context.Request.Query["measure"] == "class")
            {
                this.stopWatch.Restart();
                await this.next.Invoke(context);
                this.stopWatch.Stop();
                Console.WriteLine(
                        string.Format(
                            "MeasureMiddleware: The processing time of the request: {0}",
                            this.stopWatch.Elapsed
                        )
                    );
            }
            else
            {
                await this.next.Invoke(context);
            }
        }

    }
}
