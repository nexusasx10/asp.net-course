﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace _07.EF1.Models
{
    public class StudentModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MinLength(1)]
        [MaxLength(128)]
        [Column("Student name")]
        public string Name { get; set; }

        [Required]
        [MinLength(1)]
        [MaxLength(128)]
        [Column("Student surname")]
        public string Surname { get; set; }

        [Required]
        [MinLength(1)]
        [MaxLength(16)]
        [Column("Student group")]
        public string Group { get; set; }

        [Required]
        [Range(1, 4)]
        [Column("Student course")]
        public byte Course { get; set; }
    }
}
