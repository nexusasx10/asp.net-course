﻿using _07.EF1.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _07.EF1
{
    public interface IStudentCollectionService
    {
        bool Contains(int id);

        IEnumerable<StudentModel> GetAll();

        int Add(StudentModel student);

        StudentModel Get(int id);

        void Edit(int id, StudentModel newStudent);

        void Delete(int id);
    }

    public class StudentCollectionService : IStudentCollectionService
    {
        private StudentContext context;

        public StudentCollectionService(StudentContext context)
        {
            this.context = context;
        }

        public bool Contains(int id)
        {
            return this.context.Students.Where(x => x.Id == id).Count() > 0;
        }

        public IEnumerable<StudentModel> GetAll()
        {
            return this.context.Students;
        }

        public int Add(StudentModel student)
        {
            this.context.Students.Add(student);
            this.context.SaveChanges();
            return student.Id;
        }

        public StudentModel Get(int id)
        {
            return this.context.Students.Where(x => x.Id == id).First();
        }

        public void Edit(int id, StudentModel newStudent)
        {
            Get(id).Name = newStudent.Name;
            Get(id).Surname = newStudent.Surname;
            Get(id).Group = newStudent.Group;
            Get(id).Course = newStudent.Course;
            this.context.SaveChanges();
        }

        public void Delete(int id)
        {
            this.context.Students.Remove(Get(id));
            this.context.SaveChanges();
        }
    }
}
