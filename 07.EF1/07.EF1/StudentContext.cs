﻿using _07.EF1.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _07.EF1
{
    public class StudentContext : DbContext
    {
        public DbSet<StudentModel> Students { get; set; }

        public StudentContext(DbContextOptions<StudentContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StudentModel>().Property(p => p.Course).IsRequired();
            modelBuilder.Entity<StudentModel>().ToTable("Student");
        }

    }
}
