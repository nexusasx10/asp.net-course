﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace _07.EF1.Migrations
{
    public partial class Edited : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Surname",
                table: "Student",
                newName: "Student surname");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Student",
                newName: "Student name");

            migrationBuilder.RenameColumn(
                name: "Group",
                table: "Student",
                newName: "Student group");

            migrationBuilder.RenameColumn(
                name: "Course",
                table: "Student",
                newName: "Student course");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Student surname",
                table: "Student",
                newName: "Surname");

            migrationBuilder.RenameColumn(
                name: "Student name",
                table: "Student",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "Student group",
                table: "Student",
                newName: "Group");

            migrationBuilder.RenameColumn(
                name: "Student course",
                table: "Student",
                newName: "Course");
        }
    }
}
