﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _05.Razor.Models
{
    public class StudentListModel
    {
        public StudentListModel()
        {
            this.Students = new Dictionary<int, StudentModel>();
        }

        public Dictionary<int, StudentModel> Students { get; set; }
    }
}
