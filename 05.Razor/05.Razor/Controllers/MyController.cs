﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using _05.Razor.Models;

namespace _05.Razor.Controllers
{
    public class MyController : Controller
    {
        public static StudentListModel model = new StudentListModel();
        public static int nextID = 0;
       

        public ActionResult Index()
        {
            return View("/Views/StudentList/StudentList.cshtml", model);
        }
        
        public ActionResult Details(int id)
        {
            if (model.Students.ContainsKey(id))
            {
                return View("/Views/Student/Student.cshtml", model.Students[id]);
            }
            else
            {
                return BadRequest("There is no such student.");
            }
        }

        public ActionResult Create()
        {
            return View("/Views/CreateStudent/CreateStudent.cshtml");
        }

        [HttpPost]
        public ActionResult Create(StudentModel student)
        {
            if (ModelState.IsValid)
            {
                student.Id = nextID;
                model.Students[nextID] = student;
                nextID++;
                return View("/Views/Student/Student.cshtml", model.Students[student.Id]);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        public ActionResult Edit(int id)
        {
            if (model.Students.ContainsKey(id))
            {
                return View("/Views/EditStudent/EditStudent.cshtml", model.Students[id]);
            }
            else
            {
                return BadRequest("There is no such student.");
            }
        }

        [HttpPost]
        public ActionResult Edit(int id, StudentModel student)
        {
            if (model.Students.ContainsKey(id))
            {
                if (ModelState.IsValid)
                {
                    model.Students[id] = student;
                    return View("/Views/Student/Student.cshtml", model.Students[id]);
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            else
            {
                return BadRequest("There is no such student.");
            }
        }

        public ActionResult Delete(int id)
        {
            if (model.Students.ContainsKey(id))
            {
                return View("/Views/DeleteStudent/DeleteStudent.cshtml", model.Students[id]);
            }
            else
            {
                return BadRequest("There is no such student.");
            }
        }

        [HttpPost]
        public ActionResult Delete(int id, string name)
        {
            if (model.Students.ContainsKey(id))
            {
                if (model.Students[id].Name != name)
                {
                    return BadRequest("Wrong name.");
                }
                else
                {
                    model.Students.Remove(id);
                    return View("/Views/StudentList/StudentList.cshtml", model);
                }
            }
            else
            {
                return BadRequest("There is no such student.");
            }
        }
    }
}