﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using _03.MVC1.Models;

namespace _03.MVC1.Controllers
{
    public class MyController : Controller
    {
        public static StudentModel model = new StudentModel();
        public static int nextID;

        public ActionResult Index()
        {
            return Json(model.Students.Values);
        }
        
        public ActionResult Details(int id)
        {
            if (model.Students.ContainsKey(id))
            {
                return Json(model.Students[id]);
            }
            else
            {
                return Json("There is no such student.");
            }
        }

        public string Create(string name, string group, byte course)
        {
            Student student = new Student(nextID, name, group, course);
            model.Students[nextID] = student;
            nextID++;
            return "Created: id-" + (nextID - 1);
        }

        public string Edit(int id, string name, string group, byte course)
        {
            if (model.Students.ContainsKey(id))
            {
                Student student = new Student(id, name, group, course);
                model.Students[id] = student;
                return "Edited: id-" + id;
            }
            else
            {
                return "There is no such student.";
            }
            
        }

        public string Delete(int id)
        {
            if (model.Students.ContainsKey(id))
            {
                model.Students.Remove(id);
                return "Deleted: " + id;
            }
            else
            {
                return "There is no such student.";
            }
        }
    }
}