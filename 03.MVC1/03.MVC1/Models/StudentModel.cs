﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _03.MVC1.Models
{
    public class StudentModel
    {
        public Dictionary<int, Student> Students { get; set; }

        public StudentModel()
        {
            this.Students = new Dictionary<int, Student>();
        }
    }

    public class Student
    {
        public Student(int id, string name, string group, byte course)
        {
            this.ID = id;
            this.Name = name;
            this.Group = group;
            this.Course = course;
        }

        public int ID { get; set; }

        public string Name { get; set; }

        public string Group { get; set; }

        public byte Course { get; set; }
    }
}
