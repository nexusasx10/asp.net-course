﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace _03.MVC1.Models
{
    public class StudentModel
    {
        [Required]
        [MinLength(1)]
        [MaxLength(128)]
        public string Name { get; set; }

        [Required]
        [MinLength(1)]
        [MaxLength(128)]
        public string Surname { get; set; }

        [Required]
        [MinLength(1)]
        [MaxLength(16)]
        public string Group { get; set; }

        [Required]
        [Range(1, 4)]
        public byte Course { get; set; }
    }
}
