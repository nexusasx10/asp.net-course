﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using _03.MVC1.Models;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace _03.MVC1.Controllers
{
    public class MyController : Controller
    {
        public static Dictionary<int, StudentModel> students = new Dictionary<int, StudentModel>();
        public static int nextID;

        public ActionResult Index()
        {
            return Json(students.Values);
        }
        
        public ActionResult Details(int id)
        {
            if (students.ContainsKey(id))
            {
                return Json(students[id]);
            }
            else
            {
                return BadRequest("There is no such student.");
            }
        }

        public ActionResult Create(StudentModel student)
        {
            if (ModelState.IsValid)
            {
                students[nextID] = student;
                nextID++;
                return Json("Created: id-" + (nextID - 1));
            }
            else
            {
                return BadRequest(ModelState);
            }
            
        }

        public ActionResult Edit(int id, StudentModel student)
        {
            if (students.ContainsKey(id))
            {
                if (ModelState.IsValid)
                {
                    students[id] = student;
                    return Json("Edited: id-" + id);
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            else
            {
                return BadRequest("There is no such student.");
            }
            
        }

        public ActionResult Delete(int id)
        {
            if (students.ContainsKey(id))
            {
                students.Remove(id);
                return Json("Deleted: " + id);
            }
            else
            {
                return BadRequest("There is no such student.");
            }
        }

        [HttpPost]
        public ActionResult Details(int id, object placeholder)
        {
            return Details(id);
        }

        [HttpPost]
        public ActionResult Create(StudentModel student, object placeholder)
        {
            return Create(student);
        }

        [HttpPost]
        public ActionResult Edit(int id, StudentModel student, object placeholder)
        {
            return Edit(id, student);
        }

        [HttpPost]
        public ActionResult Delete(int id, object placeholder)
        {
            return Delete(id);
        }
    }
}