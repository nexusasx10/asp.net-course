﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using _06.DependencyInjection.Models;

namespace _06.DependencyInjection.Controllers
{
    public class HomeController : Controller
    {
        private IStudentCollectionService studentCollection;

        public HomeController(IStudentCollectionService studentCollection) // через конструктор
        {
            this.studentCollection = studentCollection;
        }

        public ActionResult Index()
        {
            return View("/Views/StudentList/StudentList.cshtml"); // через inject
        }

        public ActionResult Details(int id)
        {
            IStudentCollectionService studentCollection = HttpContext
                .RequestServices
                .GetService<IStudentCollectionService>(); // через контекст
            if (this.studentCollection.Contains(id))
            {
                return View("/Views/Student/Student.cshtml", studentCollection.Get(id));
            }
            else
            {
                return BadRequest("There is no such student.");
            }
        }

        public ActionResult Create()
        {
            return View("/Views/CreateStudent/CreateStudent.cshtml");
        }

        [HttpPost]
        public ActionResult Create(StudentModel student, [FromServices]IStudentCollectionService studentCollection) // через параметр
        {
            if (ModelState.IsValid)
            {
                int id = this.studentCollection.Add(student);
                return View("/Views/Student/Student.cshtml", studentCollection.Get(id));
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        public ActionResult Edit(int id)
        {
            if (this.studentCollection.Contains(id))
            {
                return View("/Views/EditStudent/EditStudent.cshtml", this.studentCollection.Get(id));
            }
            else
            {
                return BadRequest("There is no such student.");
            }
        }

        [HttpPost]
        public ActionResult Edit(int id, StudentModel student)
        {
            if (this.studentCollection.Contains(id))
            {
                if (ModelState.IsValid)
                {
                    this.studentCollection.Edit(id, student);
                    return View("/Views/Student/Student.cshtml", this.studentCollection.Get(id));
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            else
            {
                return BadRequest("There is no such student.");
            }
        }

        public ActionResult Delete(int id)
        {
            if (this.studentCollection.Contains(id))
            {
                return View("/Views/DeleteStudent/DeleteStudent.cshtml", this.studentCollection.Get(id));
            }
            else
            {
                return BadRequest("There is no such student.");
            }
        }

        [HttpPost]
        public ActionResult Delete(int id, string name)
        {
            if (this.studentCollection.Contains(id))
            {
                if (this.studentCollection.Get(id).Name != name)
                {
                    return BadRequest("Wrong name.");
                }
                else
                {
                    this.studentCollection.Delete(id);
                    return View("/Views/StudentList/StudentList.cshtml");
                }
            }
            else
            {
                return BadRequest("There is no such student.");
            }
        }

        public ActionResult Test(int id)
        {
            IStudentCollectionService studentCollection = ActivatorUtilities.
                CreateInstance<StudentCollectionService>(HttpContext.RequestServices); // через ActivatorUtilities
            if (studentCollection.Contains(id))
            {
                return View("/Views/Student/Student.cshtml", studentCollection.Get(id));
            }
            else
            {
                return BadRequest("There is no such student.");
            }
        }
    }
}
