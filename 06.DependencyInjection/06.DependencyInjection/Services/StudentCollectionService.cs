﻿using _06.DependencyInjection.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _06.DependencyInjection
{
    public interface IStudentCollectionService
    {
        bool Contains(int id);

        IEnumerable<StudentModel> GetAll();

        int Add(StudentModel student);

        StudentModel Get(int id);

        void Edit(int id, StudentModel newStudent);

        void Delete(int id);
    }

    public class StudentCollectionService : IStudentCollectionService
    {
        private Dictionary<int, StudentModel> students;
        private int nextId;

        public StudentCollectionService()
        {
            this.students = new Dictionary<int, StudentModel>();
            this.nextId = 0;
        }

        public bool Contains(int id)
        {
            return this.students.ContainsKey(id);
        }

        public IEnumerable<StudentModel> GetAll()
        {
            return this.students.Values;
        }

        public int Add(StudentModel student)
        {
            student.Id = nextId;
            this.students[nextId] = student;
            nextId++;
            return nextId - 1;
        }

        public StudentModel Get(int id)
        {
            return this.students[id];
        }

        public void Edit(int id, StudentModel newStudent)
        {
            this.students[id] = newStudent;
        }

        public void Delete(int id)
        {
            this.students.Remove(id);
        }
    }
}
